extends Popup

var paused = false
var pause_manager 

func _ready():
	var finder = preload("res://managers/finder/Finder.gd").new(get_tree())
	
	assert(finder.has_pauser())
	pause_manager = finder.get_pauser()

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if pause_manager.menu_pause():
			paused = !paused
			visible = !visible