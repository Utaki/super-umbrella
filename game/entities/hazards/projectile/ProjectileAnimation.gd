extends AnimationPlayer

func _enter_tree():
	if !has_animation("flying"):
		add_animation("flying", Animation.new())
	if !has_animation("dying"):
		add_animation("dying", Animation.new())

func _ready():
	play("flying")
