extends Node

const UP = Vector2(0,-1)

export(float) var gravity = 2400.0 # force acting downwards

var velocity = Vector2()

# Shortcut variables
var entity
var states
var control

onready var physics = $PhysicsState

func _entity_ready(entity):
	self.entity = entity
	self.states = entity.get_component(preload("res://entities/components/states/States.gd"))
	assert(self.states != null)
	self.control = entity.get_component(preload("res://controls/ControlBase.gd"))
	assert(self.control != null)

func push(dir):
	self.velocity += dir

func _physics_process(delta):
	
	# Updates isMoving state
	self.states.isMoving = (self.velocity.length() > 0)

	# Updates onAir state
	self.states.onFloor = entity.is_on_floor()

	# Updates lookingRight state
	if   self.velocity.x > 0:
		self.states.lookingRight = true
	elif self.velocity.x < 0:
		self.states.lookingRight = false
	else:
		pass #keep looking at where you were
	
	# Update movement
	self.velocity.y += gravity*delta
	self.velocity = entity.move_and_slide(self.velocity, UP)
	self.velocity = self.physics.compute_movement(self.velocity, self.control, delta)
	
	self.physics.update_state(self.entity)

func _compute_movement(current_velocity, delta):
	var velocity = current_velocity
	
