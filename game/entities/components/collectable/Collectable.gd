extends Area2D

#Collectable attribute ( meaning it can be taken by the player )

signal collected(body)

func _on_Collectable_body_entered(body):
	if body.is_in_group("Player"):
		emit_signal("collected",body)