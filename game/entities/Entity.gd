extends Object

static func init(entity):
	for child in entity.get_children():
		if child.has_method("_entity_ready"):
			child._entity_ready(entity)

static func get_component(entity, type):
	for child in entity.get_children():
		if child is type:
			return child
	return null

static func die(entity):
	for child in entity.get_children():
		if child is Camera2D:
			entity.remove_child(child)
			child.position = child.get_camera_position()
			entity.get_parent().add_child(child)
	entity.queue_free()
