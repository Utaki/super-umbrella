extends Node

var m_pause = false #pause from pause_menu
var s_pause = false #pause from speech_box
var c_pause = false #pause from cutscene_box
var t_pause = false #pause from transition
var finder

func _ready():
	finder = preload("res://managers/finder/Finder.gd").new(get_tree())

func cutscene_pause():
	c_pause = !c_pause
	get_tree().paused = c_pause

func speech_pause():
	if not c_pause:
		s_pause = !s_pause
		get_tree().paused = s_pause

#pauses and unpauses the speech_box
func handle_speech(pause):
	assert(finder.has_HUD("speech_box") or finder.has_HUD("cutscene_box"))
	
	if pause:
		if c_pause:
			finder.get_HUD("cutscene_box").pause_mode = PAUSE_MODE_STOP
		else:
			finder.get_HUD("speech_box").pause_mode = PAUSE_MODE_STOP
	else:
		if c_pause:
			finder.get_HUD("cutscene_box").pause_mode = PAUSE_MODE_PROCESS
		else:
			finder.get_HUD("speech_box").pause_mode = PAUSE_MODE_PROCESS

# Returns true if pause popup should be shown, false otherwise
func menu_pause():
	
	if t_pause:
		return false

	else:
		m_pause = !m_pause

		if s_pause or c_pause:
			handle_speech(m_pause)
		else:
			get_tree().paused = m_pause

		return true

func transition_pause():
	t_pause = !t_pause
	
	if s_pause or c_pause:
		handle_speech(t_pause)
	else:
		get_tree().paused = t_pause
